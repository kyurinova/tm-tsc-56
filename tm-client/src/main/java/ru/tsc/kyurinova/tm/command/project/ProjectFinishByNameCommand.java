package ru.tsc.kyurinova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractProjectCommand;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project by name...";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        projectEndpoint.finishByNameProject(sessionService.getSession(), name);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
