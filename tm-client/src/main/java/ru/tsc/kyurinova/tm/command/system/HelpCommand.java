package ru.tsc.kyurinova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

@Component
public class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display all commands with description...";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (@NotNull final AbstractCommand command : commandService.getCommands())
            System.out.println(command.name() + ": " + command.description());
    }
}
