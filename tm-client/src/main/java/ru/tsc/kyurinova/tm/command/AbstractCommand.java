package ru.tsc.kyurinova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kyurinova.tm.api.service.ICommandService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.endpoint.*;
import ru.tsc.kyurinova.tm.enumerated.Role;

public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISessionService sessionService;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    protected AdminDataEndpoint adminDataEndpoint;

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    @Override
    public String toString() {
        return super.toString();
    }

}
