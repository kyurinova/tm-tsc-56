package ru.tsc.kyurinova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

@Component
public class ArgumentsShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of arguments...";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractCommand command : commandService.getCommands()) {
            @Nullable final String argument = command.arg();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument + ": " + command.description());
        }
    }
}
