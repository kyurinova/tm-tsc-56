package ru.tsc.kyurinova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class FileScanner {

    public final static String PATH = "./";

    private static final int INTERVAL = 1;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(bootstrap.getCommandService().getArguments().stream()
                .map(AbstractCommand::name)
                .collect(Collectors.toList())
        );
        es.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && commands.contains(o.getName()))
                .forEach(o -> {
                    @NotNull final String name = o.getName();
                    try {
                        bootstrap.runCommand(name);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        o.delete();
                    }
                });
    }

}
