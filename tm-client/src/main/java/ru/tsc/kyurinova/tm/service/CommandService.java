package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.ICommandRepository;
import ru.tsc.kyurinova.tm.api.service.ICommandService;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

import java.util.Collection;

@Service
public class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@Nullable String arg) {
        if (arg == null || arg.isEmpty()) return null;
        return commandRepository.getCommandByArg(arg);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Nullable
    @Override
    public Collection<String> getListCommandName() {
        return commandRepository.getListCommandName();
    }

    @Nullable
    @Override
    public Collection<String> getListCommandArg() {
        return commandRepository.getListCommandArg();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
