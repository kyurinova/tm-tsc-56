package ru.tsc.kyurinova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

@Component
public class DeveloperInfoShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info...";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }
}
