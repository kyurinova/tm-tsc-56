package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.enumerated.Sort;
import ru.tsc.kyurinova.tm.endpoint.TaskDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskListShowCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull final List<TaskDTO> tasks;
        System.out.println("[LIST TASKS]");
        if (sort == null || sort.isEmpty())
            tasks = taskEndpoint.findAllTask(session);
        else {
            @NotNull final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskEndpoint.findAllTaskSorted(session, sort);
        }
        for (TaskDTO task : tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.getId() + ". " + task.getName() + ". " + task.getDescription() + ". " + task.getStatus() + ". " + task.getStartDate());
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
