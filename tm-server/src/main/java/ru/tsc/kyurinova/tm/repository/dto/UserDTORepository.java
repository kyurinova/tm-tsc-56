package ru.tsc.kyurinova.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class UserDTORepository extends AbstractDTORepository implements IUserDTORepository {

    @Override
    public void add(@NotNull UserDTO user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull UserDTO user) {
        entityManager.merge(user);
    }

    @Override
    public UserDTO findByLogin(@NotNull String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable
    UserDTO findByEmail(@NotNull String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager.remove(findByLogin(login));
    }

    @Override
    public void remove(@NotNull UserDTO user) {
        entityManager.remove(user);
    }

    @Override
    public @Nullable
    List<UserDTO> findAll() {
        return entityManager
                .createQuery("SELECT m FROM UserDTO m", UserDTO.class)
                .getResultList();

    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO")
                .executeUpdate();
    }

    @Override
    public @Nullable
    UserDTO findById(@NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.id= :id";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable
    UserDTO findByIndex(@NotNull Integer index) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByIndex(@NotNull Integer index) {
        entityManager.remove(findByIndex(index));
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM UserDTO m";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult().intValue();
    }

}
