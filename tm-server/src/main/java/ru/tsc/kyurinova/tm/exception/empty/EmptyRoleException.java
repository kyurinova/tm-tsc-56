package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {


    public EmptyRoleException() {
        super("Error. Role is empty.");
    }

    public EmptyRoleException(Role role) {
        super("Error." + role + " Role is empty.");
    }

}
