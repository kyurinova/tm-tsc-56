package ru.tsc.kyurinova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.model.IProjectRepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.User;

import java.util.Date;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class ProjectRepository extends AbstractRepository implements IProjectRepository {

    @Override
    public void add(@NotNull String userId, @NotNull Project project) {
        project.setUser(entityManager.find(User.class, userId));
        entityManager.persist(project);
    }

    @Override
    public @Nullable
    Project findByName(@NotNull String userId, @NotNull String name) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.name = :name and m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByName(@NotNull String userId, @NotNull String name) {
        entityManager.remove(findByName(userId, name));
    }

    @Override
    public void startById(@NotNull String userId, @NotNull String id) {
        final Project Project = findById(userId, id);
        Project.setStatus(Status.IN_PROGRESS);
        Project.setStartDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void startByIndex(@NotNull String userId, @NotNull Integer index) {
        final Project Project = findByIndex(userId, index);
        Project.setStatus(Status.IN_PROGRESS);
        Project.setStartDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void startByName(@NotNull String userId, @NotNull String name) {
        final Project Project = findByName(userId, name);
        Project.setStatus(Status.IN_PROGRESS);
        Project.setStartDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void finishById(@NotNull String userId, @NotNull String id) {
        final Project Project = findById(userId, id);
        Project.setStatus(Status.COMPLETED);
        Project.setFinishDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void finishByIndex(@NotNull String userId, @NotNull Integer index) {
        final Project Project = findByIndex(userId, index);
        Project.setStatus(Status.COMPLETED);
        Project.setFinishDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void finishByName(@NotNull String userId, @NotNull String name) {
        final Project Project = findByName(userId, name);
        Project.setStatus(Status.COMPLETED);
        Project.setFinishDate(new Date());
        entityManager.merge(Project);
    }

    @Override
    public void update(@NotNull String userId, @NotNull Project project) {
        final String projectUserId = project.getUser().getId();
        if (userId.equals(projectUserId))
            entityManager.merge(project);
    }

    @Override
    public void changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        final Project Project = findById(userId, id);
        Project.setStatus(status);
        entityManager.merge(Project);
    }

    @Override
    public void changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        final Project Project = findByIndex(userId, index);
        Project.setStatus(status);
        entityManager.merge(Project);
    }

    @Override
    public void changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        final Project Project = findByName(userId, name);
        Project.setStatus(status);
        entityManager.merge(Project);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Project project) {
        @Nullable final String projectUserId = project.getUser().getId();
        if (userId.equals(projectUserId))
            entityManager.remove(project);
    }

    @Override
    public @NotNull
    List<Project> findAll() {
        return entityManager.createQuery("SELECT m FROM Project m", Project.class)
                .getResultList();
    }

    @Override
    public @NotNull
    List<Project> findAllUserId(@NotNull String userId) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Project").
                executeUpdate();
    }

    @Override
    public void clearUserId(@NotNull String userId) {
        @NotNull final String jpql = "DELETE FROM Project m WHERE m.user.id = :userId";
        entityManager.createQuery(jpql).
                setParameter("userId", userId).
                executeUpdate();
    }

    @Override
    public @Nullable
    Project findById(@NotNull String userId, @NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId  and m.id = :id";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public @Nullable
    Project findByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final String jpql = "SELECT m FROM Project m WHERE m.user.id = :userId";
        return entityManager.createQuery(jpql, Project.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @Nullable final Project Project = findById(userId, id);
        entityManager.remove(Project);
    }
}
