package ru.tsc.kyurinova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository {

    void add(
            @NotNull final Session session
    );

    @Nullable
    public Session findById(
            @NotNull final String id
    );

    void remove(
            @NotNull final Session session
    );

    @Nullable
    List<Session> findAll(
    );

    void clear(
    );

    @Nullable
    Session findByIndex(
            @NotNull final Integer index
    );

    void removeByIndex(
            @NotNull final Integer index
    );

    void removeById(
            @NotNull final String id
    );

    int getSize(
    );

}
