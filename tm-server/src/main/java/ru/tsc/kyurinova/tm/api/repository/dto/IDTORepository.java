package ru.tsc.kyurinova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IDTORepository {

    @NotNull
    EntityManager getEntityManager();

}
