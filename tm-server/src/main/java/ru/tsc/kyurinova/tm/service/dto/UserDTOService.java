package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.*;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.exception.user.EmailExistsException;
import ru.tsc.kyurinova.tm.exception.user.LoginExistsException;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService implements IUserDTOService {

    public IUserDTORepository getRepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @NotNull
    @Autowired
    private ILogService logService;

    @Autowired
    private IPropertyService propertyService;

    @Override
    public void addAll(@NotNull final List<UserDTO> users) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (UserDTO user : users) {
                userRepository.add(user);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }

    }


    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable UserDTO user;
        try {
            user = userRepository.findByLogin(login);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable UserDTO user;
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findByEmail(email);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @NotNull final UserDTO user = userRepository.findByLogin(login);
            if (user != null) throw new LoginExistsException();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @NotNull final UserDTO user = userRepository.findByEmail(email);
            if (user != null) throw new EmailExistsException();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = findByLogin(login);
            if (user != null) {
                entityManager.getTransaction().begin();
                userRepository.removeByLogin(login);
                entityManager.getTransaction().commit();
            }
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO createAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.ADMIN);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            System.out.println("before add");
            userRepository.add(user);
            System.out.println("add OK");
            entityManager.getTransaction().commit();
            System.out.println("commit OK");
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        isLoginExists(login);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new EmptyUserIdException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        @Nullable final UserDTO user = findById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("Y");
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty())
            throw new EmptyLoginException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) return null;
        user.setLocked("N");
        try {
            entityManager.getTransaction().begin();
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void remove(@Nullable final UserDTO entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(entity);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findAll();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findById(id);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByIndex(index);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = findById(id);
            if (user != null) {
                entityManager.getTransaction().begin();
                userRepository.removeById(id);
                entityManager.getTransaction().commit();
            }
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            @Nullable final UserDTO user = findByIndex(index);
            if (user != null) {
                entityManager.getTransaction().begin();
                userRepository.removeByIndex(index);
                entityManager.getTransaction().commit();
            }
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return (userRepository.findById(id) != null);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsByIndex(@NotNull final Integer index) {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return (userRepository.findByIndex(index) != null);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() {
        @NotNull final IUserDTORepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.getSize();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }
}
