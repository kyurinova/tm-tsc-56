package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.marker.UnitCategory;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserRepositoryTest {

    /*@NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.ru";

    public EntityManager GetEntityManager() {
        return entityManager;
    }

    public UserRepositoryTest() {
        user = new UserDTO();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt("testUser", 3, "test"));
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserTest() {
        Assert.assertEquals(user.getId(), userRepository.findById(userId).getId());
        Assert.assertEquals(user.getId(), userRepository.findByIndex(0).getId());
        Assert.assertEquals(user.getId(), userRepository.findByLogin(userLogin).getId());
        Assert.assertEquals(user.getId(), userRepository.findByEmail(userEmail).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeUserTest() {
        Assert.assertNotNull(user);
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByLoginTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userLogin);
        entityManager.getTransaction().begin();
        userRepository.removeByLogin(userLogin);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByIdTest() {
        Assert.assertNotNull(user);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        userRepository.removeById(userId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeByIdIndex() {
        Assert.assertNotNull(user);
        entityManager.getTransaction().begin();
        userRepository.removeByIndex(0);
        entityManager.getTransaction().commit();
        Assert.assertNull(userRepository.findByIndex(0));
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }*/

}
